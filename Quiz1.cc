/* 
    Nama  : Rahayu Febrianti
    NIM   : 301230057
    Kelas : 1B Teknik Informatika
    Quiz  : Mengolah Data
*/

#include <iostream>

using namespace std;

int main()
{
    int i, sum = 0, count = 0;
    float avarage;

    do
    {
        cout << " Masukan Data ke-" << count + 1 <<" : ";
        cin >> i;

        if (i!=0)
        {
            sum += i;
            count++;
        }
    }
    while (i!=0);
    {
        if (count!=0)
        {
            avarage = static_cast<float>(sum)/count;
            cout << "Banyaknya Nilai Data : "<< count << endl;
            cout << "Total Nilai Data : " << sum << endl;
            cout << "Rata-rata Nilai Data : "<< avarage << endl;

        }
    }
    return 0;
}
