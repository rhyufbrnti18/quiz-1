#include <iostream>
using namespace std;

// Function to calculate factorial
int factorial(int X) {
    int result = 1;
    for (int i = 1; i <= X; ++i) {
        result *= i;
    }
    return result;
}

// Function to calculate permutations
int calculatePermutations(int n, int r) {
    return factorial(n) / factorial(n - r);
}

// Function to calculate combinations
int calculateCombinations(int n, int r) {
    return factorial(n) / (factorial(r) * factorial(n - r));
}

// Procedure to display result
void displayResult(char operation, int n, int r, int result) {
    if (operation == 'p') {
        cout << "Permutasi dari " << n << " dan " << r << " adalah " << result << endl;
    } else if (operation == 'k') {
        cout << "Kombinasi dari " << n << " dan " << r << " adalah " << result << endl;
    }
}

int main() {
    int n, r;
    char operation;

    cout << "=======================" << endl;
    cout << "PERMUTASI DAN KOMBINASI" << endl;
    cout << "=======================" << endl << endl;
    cout << "Permutasi atau Kombinasi ? (p/k) : ";
    cin >> operation;
    cout << endl;
    cout << "Tentukan nilai n : ";
    cin >> n;
    cout << "Tentukan nilai r : ";
    cin >> r;
    cout << endl;

    switch (operation) {
        case 'p':
            displayResult(operation, n, r, calculatePermutations(n, r));
            break;

        case 'k':
            displayResult(operation, n, r, calculateCombinations(n, r));
            break;
    }

    return 0;
}
